# Contributing to sOSage

You're very welcome to contribute code to sOSage! Feel free to fork and send merge requests!

**Note:**

Please bear in mind that I've yet to add all of my own code to this repo. I'm currently reviewing the code, most of which I originally wrote in 2018. I'm copying the code into this repo as I review.

I'll remove / amend this note once the process is complete and everything on here is up-to-date.

