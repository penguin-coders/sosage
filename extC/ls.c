/************************************************************************
 * sOSage - an example OS for educational/hobbyist purposes              *
 * Copyright (C) 2022 Mark Williams                                      *
 *                                                                       *
 * This file is part of sOSage.                                          *
 *                                                                       *
 * sOSage is free software: you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * sOSage is distributed in the hope that it will be useful,             *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      *
 *************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sFS/index.h>
#include <sFS/fs.h>

int main() {
	
	int i;
	char bf[10];
	FILE *fsfile;
	filesystem fs;
	index_blk indx;
	unsigned int blk;

	// Read in filesystem struct
	fsfile = fopen(FSFILE, "r+b");
	fseek(fsfile, BLKSIZE*OSSECTORS, SEEK_SET); // skip over boot sector
	fread(&fs, sizeof(fs), 1, fsfile);
	
	// Get the index block
	blk = get_last_index_blk(&fs);
	//printf("Index block : %d\n", blk);

	// Read in the index
	read_index_blk(fsfile, blk, &indx);
	for(i=0; i<indx.next_rec; i++) {
		//	printf("%s : %u : %u\n", indx.recs[i].filename, indx.recs[i].start_blk, indx.recs[i].length);
		printMsg(indx.recs[i].filename); crlf();
	}

	return 0;
}
