/************************************************************************
 * sOSage - an example OS for educational/hobbyist purposes              *
 * Copyright (C) 2022 Mark Williams                                      *
 *                                                                       *
 * This file is part of sOSage.                                          *
 *                                                                       *
 * sOSage is free software: you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * sOSage is distributed in the hope that it will be useful,             *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      *
 *************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "testFileIO.h"

void main()
{

	char buff[10];
	FILE *f;
	testStruct ts;
	int c;

	// Read in sector 17, which should start with a testStruct
	// N.B. The file testStruct contains a testStruct and can be copied to the filesystem with cpin
	f = fopen("17", "r+b");
	
	itoa10(f->start_sector,buff); printMsg("start_sector : "); printMsg(buff); crlf();
	itoa10(f,buff); printMsg("f : "); printMsg(buff); crlf();
	itoa10(sizeof(ts),buff); printMsg("ts size : "); printMsg(buff); crlf();

	fseek(f, 10, SEEK_SET);

	for(c=0; c<=2; c++) {
		fread(&ts, sizeof(ts), 1, f);
		crlf();
		itoa10(ts.a,buff); printMsg("ts.a : "); printMsg(buff); crlf();
		itoa10(ts.b,buff); printMsg("ts.b : "); printMsg(buff); crlf();
		printMsg("ts.str : "); printMsg(ts.str); crlf();
		crlf();
	}
	
	ts.a = c;
	fwrite(&ts, sizeof(ts), 1, f);
	
}
