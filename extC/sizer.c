#include <stdio.h>
#include <stdlib.h>

int main() {

	int buff[10];

	itoa10(sizeof(char),buff); printMsg("char : "); printMsg(buff); crlf();	
	itoa10(sizeof(short),buff); printMsg("short : "); printMsg(buff); crlf();	
	itoa10(sizeof(int),buff); printMsg("int : "); printMsg(buff); crlf();	
	itoa10(sizeof(long),buff); printMsg("long : "); printMsg(buff); crlf();	
	itoa10(sizeof(FILE),buff); printMsg("FILE : "); printMsg(buff); crlf();	
	crlf();

	itoa10(sizeof(char*),buff); printMsg("char* : "); printMsg(buff); crlf();	
	itoa10(sizeof(short*),buff); printMsg("short* : "); printMsg(buff); crlf();	
	itoa10(sizeof(int*),buff); printMsg("int* : "); printMsg(buff); crlf();	
	itoa10(sizeof(long*),buff); printMsg("long* : "); printMsg(buff); crlf();	
	itoa10(sizeof(FILE*),buff); printMsg("FILE* : "); printMsg(buff); crlf();	

}
