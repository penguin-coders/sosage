#!/usr/bin/perl -w

#########################################################################
# sOSage - an example OS for educational/hobbyist purposes              #
# Copyright (C) 2022 Mark Williams                                      #
#                                                                       #
# This file is part of sOSage.                                          #
#                                                                       #
# sOSage is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# sOSage is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
# GNU General Public License for more details.                          #
#                                                                       #
# You should have received a copy of the GNU General Public licenses    #
# along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      #
#########################################################################

my $top =  <<'TOP';
%include	'../asm/sectors.asm'
org	0x7c00 + sectors*512		; Address code will be loaded - calculated from number of OS sectors
bits	16					         ; Tell nasm we want to code for 16 bit real mode

%assign cwrap 0x7e00
TOP
print $top;

# addresses of cwrapper functions
my @arr = qw(_printMsg _crlf _itoa10 _strcpy _strrchr _memcpy _fopen _fread _fseek _fwrite _read_index_blk _get_last_index_blk);
for my $i (0..$#arr) {
	print "%assign cwrap".$arr[$i]." cwrap+(3*$i)\n";
}

# jump to main functions
print "\njmp _main\n\n";

