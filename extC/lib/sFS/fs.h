/************************************************************************
 * sOSage - an example OS for educational/hobbyist purposes              *
 * Copyright (C) 2022 Mark Williams                                      *
 *                                                                       *
 * This file is part of sOSage.                                          *
 *                                                                       *
 * sOSage is free software: you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * sOSage is distributed in the hope that it will be useful,             *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      *
 *************************************************************************/

#ifndef SOSEXE
#define SOSEXE 
asm("%include 'lib/sosexe.asm'");
#endif

#ifndef FSFILE

#define FSFILE "sosage.img"
#define FSVERSION 1
#define BLKSIZE 512
#define NUMBLKS 2880
#define FSMAP NUMBLKS / 8
#define BITSPERBLK 9
#define FSPTRS NUMBLKS * BITSPERBLK / 8
#ifndef OSSECTORS
#define OSSECTORS 0
#endif

typedef struct {
	char magic_num[4];	
	unsigned short int version;
	unsigned short int block_size;
	unsigned short int num_blocks;
	unsigned short int index_block;
	unsigned char fsmap[FSMAP];
	unsigned char fsptrs[FSPTRS];
	char end[4];
} filesystem;

typedef union {
	unsigned short	n;
	unsigned char ch[2];
} numchar;

unsigned int pow2(); //unsigned int n);
void set_bit(); //unsigned char *pchar, unsigned short int bitnum);
void clear_bit(); //unsigned char *pchar, unsigned short int bitnum);
void mark_block_used(); //filesystem *fs, int blk);
void mark_block_free(); //filesystem *fs, int blk); 
void set_block_ptr(); //filesystem *fs, unsigned int blk, unsigned int val); 
int next_free_block(); //filesystem *fs);
int next_file_block(); //filesystem *fs, unsigned int curr_blk);

#endif
