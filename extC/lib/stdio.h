/************************************************************************
 * sOSage - an example OS for educational/hobbyist purposes              *
 * Copyright (C) 2022 Mark Williams                                      *
 *                                                                       *
 * This file is part of sOSage.                                          *
 *                                                                       *
 * sOSage is free software: you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * sOSage is distributed in the hope that it will be useful,             *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      *
 *************************************************************************/

#ifndef SOSEXE
#define SOSEXE 
asm("%include 'lib/sosexe.asm'");
#endif

#ifndef __STDIO_H
#define __STDIO_H

/* typedefs */
typedef struct {
	char buff[512];
	int start_sector;
	int offset_bytes;
} FILE;

typedef unsigned int size_t;

/* defines */
#define SEEK_SET	0	/* Seek from beginning of file.  */
#define SEEK_CUR	1	/* Seek from current position.  */
#define SEEK_END	2	/* Seek from end of file.  */


/* functions */
void printMsg(str) 
char str[];
{
	asm("jmp 0x0000:cwrap_printMsg");
}

void crlf()
{
	asm("jmp 0x0000:cwrap_crlf");
}

/*	
 *	These functions no longer need to be accessed directly
 *	can probably delete at some point
void readWriteSectors(addr,sector_num,sectors_to_read)
int *addr; 
int sector_num;
int sectors_to_read;
{
	asm("jmp 0x0000:cwrap_readWriteSectors");
}

void writeSectors(addr,sector_num,sectors_to_read)
int *addr; 
int sector_num;
int sectors_to_read;
{
	asm("jmp 0x0000:cwrap_writeSectors");
}
*/

FILE *fopen(filename, mode)
char *filename;
char *mode;
{
	asm("jmp 0x0000:cwrap_fopen");
}

char *memcpy(a,b,bytes) 
char a[];
char b[];
int bytes;
{
	asm("jmp 0x0000:cwrap_memcpy");
}

size_t fread(ptr, size, nmemb, stream)
char *ptr; 
size_t size; 
size_t nmemb; 
FILE *stream;
{
	asm("jmp 0x0000:cwrap_fread");
}

size_t fwrite(ptr, size, nmemb, stream)
char *ptr; 
size_t size; 
size_t nmemb; 
FILE *stream;
{
	asm("jmp 0x0000:cwrap_fwrite");
}

int fseek(stream, offset, whence)
FILE *stream;
int offset; 
int whence;
{
	asm("jmp 0x0000:cwrap_fseek");
}
	
#endif
