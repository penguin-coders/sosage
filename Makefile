sosage.img: asm/*.asm C/*.asm sFS/format sFS/OScode/sFS.asm extC/ls
	nasm -f bin asm/sosage.asm -o sosage.img
	./do_dd.sh
	sFS/format
	sFS/cpin extC/ls

C/*.asm:
	cd C && $(MAKE)

sFS/format:
	cd sFS && $(MAKE)

sFS/OScode/sFS.asm:
	cd sFS/OScode && $(MAKE)

extC/ls:
	cd extC && $(MAKE) 
	
clean:
	rm sosage.img
	cd C && $(MAKE) clean
	cd sFS && $(MAKE) clean
	cd sFS/OScode && $(MAKE) clean
	cd extC && $(MAKE) clean

