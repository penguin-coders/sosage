/************************************************************************
 * sOSage - an example OS for educational/hobbyist purposes              *
 * Copyright (C) 2022 Mark Williams                                      *
 *                                                                       *
 * This file is part of sOSage.                                          *
 *                                                                       *
 * sOSage is free software: you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * sOSage is distributed in the hope that it will be useful,             *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      *
 *************************************************************************/

char *strcpy(a,b) 
char a[];
char b[];
{
	int i = 0;
	if(b>0) {
		while((a[i] = b[i]) != '\0')
			i++;
	}
	return a;
}

char *strrchr(str, c)
char str[];
int c;
{
	int len = 0; 
	int i;
	char ch = (char) c;
	
	while(str[len]!='\0') {
		len++;
	}
	
	for(i=len-1; i>=0; i--) {
		if(str[i]==ch) {
			return &str[i];
		}
	}

	return '\0';
}

