/************************************************************************
 * sOSage - an example OS for educational/hobbyist purposes              *
 * Copyright (C) 2022 Mark Williams                                      *
 *                                                                       *
 * This file is part of sOSage.                                          *
 *                                                                       *
 * sOSage is free software: you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * sOSage is distributed in the hope that it will be useful,             *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      *
 *************************************************************************/

int getCmdNum(cmdBuf, cmdList) 
char *cmdBuf, *cmdList;
{

	int cmdNum = 0;
	int offset;
	char bufCh,listCh;

	while(*(cmdList+8*cmdNum)!=0) {
		if(*(cmdList+8*cmdNum)==*cmdBuf) {
			offset = 0;
			while(*(cmdList+8*cmdNum+offset)==*(cmdBuf+offset)) {
				bufCh = *(cmdBuf+offset);
				if(bufCh==0x20)
					break;
				offset++;
			}
			listCh = *(cmdList+8*cmdNum+offset);
			bufCh = *(cmdBuf+offset);
			if((bufCh==0 || bufCh==0x20 || bufCh==0x0d) && (listCh==0 || listCh==0x20))
				break;
		}
		cmdNum++;
	}

	return cmdNum;
}

