/************************************************************************
 * sOSage - an example OS for educational/hobbyist purposes              *
 * Copyright (C) 2022 Mark Williams                                      *
 *                                                                       *
 * This file is part of sOSage.                                          *
 *                                                                       *
 * sOSage is free software: you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * sOSage is distributed in the hope that it will be useful,             *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      *
 *************************************************************************/

#include "../sFS/index.h"
#include "../sFS/fs.h"

int findCmd(cmd,fname) 
char cmd[];
char fname[];
{
	int i=0;
	while(*(fname+i)!='\0' && *(cmd+i)==*(fname+i))
		i++;

	if(*(fname+i)=='\0') {
		return 0;
	} else { 
		return 1;
	}
}

int runExternalCmd(cmdBuf) 
char *cmdBuf;
{
	
	int i;
	char bf[10];
	FILE *fsfile;
	filesystem fs;
	index_blk indx;
	unsigned int blk;

	int load_addr = 0x7c00+512*OSSECTORS;

	// Read in filesystem struct
	fsfile = fopen(FSFILE, "r+b");
	fseek(fsfile, BLKSIZE*OSSECTORS, SEEK_SET); // skip over boot sector
	fread(&fs, sizeof(fs), 1, fsfile);
	
	// Get the index block
	blk = get_last_index_blk(&fs);
	
	// Read in the index
	read_index_blk(fsfile, blk, &indx);
	for(i=0; i<indx.next_rec; i++) {
		// Lookup command in index
		if(findCmd(cmdBuf,indx.recs[i].filename)==0) {
			readLinearBlocks(load_addr, indx.recs[i].start_blk, 1);
			crlf();
			run_code_at_addr(load_addr);
			return 0;
		}
	}
	
	return 1;
}
