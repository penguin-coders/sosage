#include <stdio.h>
#include "getCmdNum.c"
#include "itoa10.c"

char cmdList[] = "quit    ls      poke    dump    save    savd    run     ";

void main(int argc, char *argv[]) {

	int cmdNum;
	char buf[10];

	if(argc==2) {
		cmdNum = getCmdNum(argv[1], cmdList);
		printf("Int: Command number %d\n", cmdNum);
		itoa10(cmdNum,buf);
		printf("String: Command number %s\n", buf);
	}

}
