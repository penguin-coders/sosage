#!/usr/bin/perl -w

#########################################################################
# sOSage - an example OS for educational/hobbyist purposes              #
# Copyright (C) 2022 Mark Williams                                      #
#                                                                       #
# This file is part of sOSage.                                          #
#                                                                       #
# sOSage is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# sOSage is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
# GNU General Public License for more details.                          #
#                                                                       #
# You should have received a copy of the GNU General Public licenses    #
# along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      #
#########################################################################

while(<>) {
	
	# Remove #s from labels
	s|#(\.\d+)|$1|;
	
	# Convert format for adding bytes of data
	s#\.(ascii|byte)#db#;

	# Substitute weird mnemonics for nasm-friendly ones
	s|^br\s|jmp |;
	s|^bne\s|jne |;
	s|^ble\s|jle |;
	s|^blo\s|jl |;
	
	# Add in indentation
	s|^([a-zA-Z\.].*)$|\t$1| unless m|^[\%\t]|;
	
	# Convert hex char format
	s|\$([\dA-Fa-f]+)|0x$1|;
	
	# Convert offset formats
	s|-([x\da-fA-F]+)\[(\w+)\]|[$2-$1]|;
	s|([x\da-feA-F]+)\[(\w+)\]|[$2+$1]|;
	
	# Remove *s from immediate values
	s#(ax|bx|cx|dx|si|di|sp|bp|ah|bh|ch|dh|al|bl|cl|dl),\*(-?\d+)#$1,$2#;
	
	# Remove #s from hex values
	s/(ax|bx|cx|dx|si|di|sp|bp|ah|bh|ch|dh|al|bl|cl|dl),\#/$1,/;

	# Add 'word' when pushing value from stack
	s|push\t\[|push\tword [|;
	
	print if !m#^\t?(!|export|\.data|\.bss|\.\.FFFF)#;
}

