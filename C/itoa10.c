/************************************************************************
 * sOSage - an example OS for educational/hobbyist purposes              *
 * Copyright (C) 2022 Mark Williams                                      *
 *                                                                       *
 * This file is part of sOSage.                                          *
 *                                                                       *
 * sOSage is free software: you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * sOSage is distributed in the hope that it will be useful,             *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      *
 *************************************************************************/

/*
 * This function has been compiled and moved to the asm folder
 * as it needed some manual tweaks
 *
unsigned int div(a, b, mod)
unsigned int a;
unsigned int b;
unsigned int *mod;
{
	unsigned int q = a / b;
	*mod = getDX();
	return q;
}
*/

char *itoa10(n, str) 
unsigned int n;
char str[];
{
	unsigned int c = 0;
	unsigned int i, imax, newn, digit;
	char ch;

	if(n==0) {
		str[0] = '0'; str[1] = '\0';
		return str;
	}

	while(n>0) {
		//newn = n / 10;
		//digit = getDX(); // n%10 will be in dx register
		newn = div(n,10,&digit);
		str[c++] = (char) digit+48; 
		n = newn;
	}
	str[c] = '\0';
	// reverse string
	imax = c--/2;
	for(i=0; i<imax; i++) {
		ch = str[c-i];
		str[c-i] = str[i];
		str[i] = ch;
	}

	return str;
}
