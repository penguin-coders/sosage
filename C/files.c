/************************************************************************
 * sOSage - an example OS for educational/hobbyist purposes              *
 * Copyright (C) 2022 Mark Williams                                      *
 *                                                                       *
 * This file is part of sOSage.                                          *
 *                                                                       *
 * sOSage is free software: you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * sOSage is distributed in the hope that it will be useful,             *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      *
 *************************************************************************/

#define SEEK_SET	0	/* Seek from beginning of file.  */
#define SEEK_CUR	1	/* Seek from current position.  */
#define SEEK_END	2	/* Seek from end of file.  */

typedef struct {
	char buff[512];
	int start_sector;
	int offset_bytes;
} FILE;

typedef unsigned int size_t;

int min(a,b)
int a, b;
{
	if(a<b) {
		return a;
	} else {
		return b;
	}
}

FILE *fopen(filename, mode)
char *filename;
char *mode;
{
	FILE *f;

	// We don't have malloc yet, so hard-wire an address
	f = 0x9000;

	f->start_sector = 0;
	f->offset_bytes = 0;

	return f;
}

char *memcpy(a,b,bytes) 
char a[];
char b[];
int bytes;
{
	int i = 0;
	for(i=0;i<bytes;i++) {
		a[i] = b[i];
	}
	return a;
}

void readWriteLinearBlocks(buff, linear_sector, sector_count, function)
char *buff;
int linear_sector, sector_count;
{

	int cylinder, head, sector, r;

	cylinder = div(linear_sector,36,&r);
	head = div(linear_sector,18,&sector);
	sector++;

	readWriteSectors(buff, cylinder, head, sector, sector_count, function);
}

void readLinearBlocks(buff, linear_sector, sector_count)
char *buff;
int linear_sector, sector_count;
{
	readWriteLinearBlocks(buff, linear_sector, sector_count, 2);
}

void writeLinearBlocks(buff, linear_sector, sector_count)
char *buff;
int linear_sector, sector_count;
{
	readWriteLinearBlocks(buff, linear_sector, sector_count, 3);
}

size_t fread(ptr, size, nmemb, stream)
char *ptr; 
size_t size; 
size_t nmemb; 
FILE *stream;
{

	int total_bytes_to_copy, first_sector, skip_bytes, first_sector_bytes, first_whole_sector, whole_sectors, last_sector_bytes;

	total_bytes_to_copy = size*nmemb;
	
	first_sector = stream->start_sector + stream->offset_bytes / 512;
	skip_bytes = stream->offset_bytes % 512;

	last_sector_bytes = (total_bytes_to_copy + skip_bytes) % 512;

	whole_sectors = ((total_bytes_to_copy + skip_bytes) / 512)+1;
	if(skip_bytes>0) 
		whole_sectors -= 1;
	if(last_sector_bytes>0) 
		whole_sectors -= 1;

	if(skip_bytes>0) {
		first_sector_bytes = min(512-skip_bytes,total_bytes_to_copy);
		first_whole_sector = first_sector+1;
	} else {
		first_sector_bytes = 0;
		first_whole_sector = first_sector;
	}

	if(first_sector_bytes>0) {
		readLinearBlocks(stream->buff, first_sector, 1);
		memcpy(ptr, stream->buff+skip_bytes, first_sector_bytes);
	}

	if(whole_sectors>0) {
		readLinearBlocks(ptr+first_sector_bytes, first_whole_sector, whole_sectors);
	}
	 
	if(last_sector_bytes>0) {
		readLinearBlocks(stream->buff, first_whole_sector+whole_sectors, 1);
		memcpy(ptr+first_sector_bytes+whole_sectors*512,stream->buff,last_sector_bytes);
	}

	// Update offset
	stream->offset_bytes += total_bytes_to_copy;

	return total_bytes_to_copy;
}

int fseek(stream, offset, whence)
FILE *stream;
int offset; 
int whence;
{
	switch(whence) {
		case SEEK_SET: 
			stream->offset_bytes = offset; break;
		case SEEK_CUR: 
			stream->offset_bytes += offset; break;
		case SEEK_END: // not yet implemented
			break;
	}
}

size_t fwrite(ptr, size, nmemb, stream)
char *ptr; 
size_t size; 
size_t nmemb; 
FILE *stream;
{

	int total_bytes_to_copy, first_sector, skip_bytes, first_sector_bytes, first_whole_sector, whole_sectors, last_sector_bytes;

	total_bytes_to_copy = size*nmemb;
	
	first_sector = stream->start_sector + stream->offset_bytes / 512;
	skip_bytes = stream->offset_bytes % 512;

	last_sector_bytes = (total_bytes_to_copy + skip_bytes) % 512;

	whole_sectors = ((total_bytes_to_copy + skip_bytes) / 512)+1;
	if(skip_bytes>0) 
		whole_sectors -= 1;
	if(last_sector_bytes>0) 
		whole_sectors -= 1;

	if(skip_bytes>0) {
		first_sector_bytes = min(512-skip_bytes,total_bytes_to_copy);
		first_whole_sector = first_sector+1;
	} else {
		first_sector_bytes = 0;
		first_whole_sector = first_sector;
	}

	if(first_sector_bytes>0) {
		readLinearBlocks(stream->buff, first_sector, 1);
		memcpy(stream->buff+skip_bytes, ptr, first_sector_bytes);
		writeLinearBlocks(stream->buff, first_sector, 1);
	}

	if(whole_sectors>0) {
		writeLinearBlocks(ptr+first_sector_bytes, first_whole_sector, whole_sectors);
	}
	 
	if(last_sector_bytes>0) {
		readLinearBlocks(stream->buff, first_whole_sector+whole_sectors, 1);
		memcpy(stream->buff,ptr+first_sector_bytes+whole_sectors*512,last_sector_bytes);
		writeLinearBlocks(stream->buff, first_whole_sector+whole_sectors, 1);
	}

	// Update offset
	stream->offset_bytes += total_bytes_to_copy;

	return total_bytes_to_copy;
}

/*
int fclose(stream)
FILE *stream;
{
	return 0;
}
*/
