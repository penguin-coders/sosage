# sOSage

**Version 0.3.1** - [Change log](CHANGELOG.md)

sOSage is an example OS for educational / hobbyist purposes. It runs on x86 architecture in real mode.

## Compiling, assembling and running sOSage

The source code for sOSage is a combination of C and assembler, and there are also some Perl scripts which are needed for building sOSage. The recommended assembler for assemling sOSage is nasm, the [Netwide Assembler](https://nasm.us) and for compiling the C code you will need Bruce's C compiler (bcc). N.B. gcc is also required for compiling some utility programs - these are not part of sOSage, but are used in building sOSage, so gcc is also essential. Makefiles are provided, so as long as you have nasm, bcc, gcc and perl installed, you can build the project by typing `make`.

sOSage compiles and assembles to a file which can be used as a floppy disk image. After assembly, the image file is zero-padded to the size of an actual floppy disk using the file using the `dd` command with the seek option. A format program is then run which writes out the filesystem structures of sFS (sOSage FileSystem). All of these steps are included in the Makefile.

Once sOSage has been built, you can boot it using virtualisation tools such as Virtualbox or [QEMU](https://www.qemu.org). 

A shell script is provided for starting sOSage with QEMU (which you may need to install first). To start sOSage, just type `./sOSboot.sh` in your terminal. To start sOSage with the QEMU monitor in your shell, type `./sOSboot.sh -m` instead.

You can also write the image file to a physical floppy disk, if you still have any.

## sOSage commands, programs and executables

The following commands are available at the sOSage command line:

- **quit** : halts the machine
- **lscmd** : lists available commands
- **poke** : pokes a value into memory (e.g. `poke 81cf 65` to add the value 65 at address 0x81cf)
- **dump** : dumps memory, specified by parameters (e.g. `dump 7c00 64` will dump 64 bytes starting at 0x7c00)
- **save** : saves a sector from RAM back to the floppy disk image (e.g. `save 4` will save the 4th sector)
- **load** : loads a sector from disk into RAM (e.g. `load 8C00 16` will load in the 16th sector at address 0x8C00)
- **run** : run the code at the location specified by the parameter (e.g. `run 8C00` will run the code at address 0x8C00)

**N.B. The load and save commands assume cylinder=0, head=0**

The above commands are built-in to the operating system, whereas the following are external commands, available in the extC folder:

- **ls** : lists files in the filesystem

These programs are also available at the sOSage command line and are included in the sOSage image file by default (by virtue of Makefile calls to `cpin`). 

In general, if the name of a program is typed at the command prompt, the shell will find the program in the filesystem's index file and load and run it. This also works for any user executables which have been copied into the filesystem using `cpin` (provided they have been compiled for sOSage).

## Adding C programs to the disk image

Some example C programs are provided in the 'extC' folder. To compile the program, you'll need:

- **bcc** : Bruce's C compiler. This is required rather than gcc, because we need to compile 16-bit code.
- **perl** : A perl script is used to process the compiler output before it is assembled.
- **nasm** : You should already have this if you've compiled sOSage.
- **make** : It's best to compile using `make` as it's not a direct compilation; bcc compiles to assembler, which is amended by the perl script before being assembled by nasm.

The perl script sets an appropriate load address for programs by including an `org` instuction. The address is calculated from the number of sectors specified in asm/sectors.asm. The same address will be used by the shell when loading programs, again calculated from asm/sectors.asm.

To copy external C programs into the image file, the `cpin` command in the **sFS** folder can be used. This will also update the filesystem structures.

## A note on C libraries

To port the C language to a new platform, it's necessary not just to have a compiler for that platform, but also a C library. The header files we normally include in C programs, such as stdio.h, stdlib.h, string.h etc, are part of the standard C library.

The C standard library forms a bridge between C application programs and the operating system. The application will call library functions, which in turn will call operating system functions. On Linux, for example, calling `printf()` from your C programs will ultimately result in calls to Linux's `write()` function, which makes a system call to the kernel. (The write system call writes bytes to a file, which in printf's case is probably stdout. In Linux, everything really is a file.)  

Normally, standard library functions are compiled into object code, and `#include` directives are used in application programs to tell the compiler that the corresponding object code needs to be linked in when compiling the program. The .h header files which are included with `#include` declare the functions whereas the function definitions will be in separate .c files. (N.B. On your main OS, it's likely that you will only have the .h files and pre-compiled object files for your C library, though for open source platforms, the .c source files will be available separately - on Linux, they'll be in the source package for glibc.)

The key point to note here, is that although C libraries are OS-specific, they are not part of the OS itself. Their object code gets merged into applications at compile time and forms part of user-space.

## The sOSage C library

There's no native C compiler for sOSage (at least, not yet). C programs must be compiled outside of sOSage, and the resulting binaries copied in to the filesystem image. However, when compiling C code for sOSage, we can't rely on the host operating system's C library, because this simply won't be there when we boot into sOSage. It's therefore been necessary to come up with sOSage implementations of some of the standard library functions, which are used, for example, in the filesystem code.

So far in sOSage, library functions have been implemented as and when required by C programs which are part of sOSage, and may not be complete implementations of those functions. For example, there is a sOSage version of stdio.h, but it doesn't have printf, and it has a version of fseek which is incomplete, but which works for the things we currently need fseek for.

The main difference in sOSage is that where C library functions have been implemented, they have been compiled into the OS and can be called directly from user programs. There are still header files that need to be included, but instead of declaring the functions, sOSage header files contain 'fake' function definitions. These 'fake' functions consist only of assembly language jmp instructions to jump to the real functions in the OS.

This is an unorthodox approach, but it works, and it's a useful hack to avoid / delay the need for a proper linker.

## sFS - The sOSage filesystem

As of version 0.2.3, sOSage has a very basic filesystem. 

The sFS folder contains all filesystem-related code, including several utility programs which are useful for running working with sOSage image files.

The utility programs are as follows:

- **format** : sets up the filesystem within the image file. This can be run after the image has been created with nasm (and extended with dd), because it will skip over the OS sectors at the start of the image when writing out the filesystem structures.
- **cpin** : copies a file block-wise into filesystem, updates filesystem structures (i.e. free space map & filesystem pointers), and creates an index file entry.
- **lsindex** : lists files within the image file with index file entries.

N.B. The format command is called from within the Makefile, so won't usually need to be run manually.

The above utility programs should be run outside of sOSage and in conjunction with a sOSage image file called sosage.img. They should be compiled with **gcc** rather than bcc, but this is handled in the Makefiles. 

The sFS code which is included in the OS is built in a sub-folder called OScode. In this folder, there is a separate Makefile, which used bcc to compile the sFS code into an assembler file which gets included in sosage.asm.

The following files are common to the OS code and the utilities:
- fs.h and fs.c : define filesystem structures and functions
- index.h and index.h : define the structure of the index file and associated funtions

## Debugging

The debug routine in debug.asm prints out the current values of AD, BX, CX, DX and DI CPU registers.

To use it, just add the following line of code at the relevant point(s) in the code: `call debug`. 

Debugging can then be toggled by commenting or uncommenting the `ret` command at the start of the debug routine.

For debugging C code, the **printMsg** function can be called.

## Licence and copyright

All sOSage code is released under the GNU General Public Licence v3.0. See [LICENCE](LICENSE) for further information.

&copy; 2020-2022 Mark Williams 
