;-----------------------------------------------------------------------;
; sOSage - an example OS for educational/hobbyist purposes              ;
; Copyright (C) 2020 Mark Williams                                      ;
;                                                                       ;
; This file is part of sOSage.                                          ;
;                                                                       ;
; sOSage is free software: you can redistribute it and/or modify        ;
; it under the terms of the GNU General Public License as published by  ;
; the Free Software Foundation, either version 3 of the License, or     ;
; (at your option) any later version.                                   ;
;                                                                       ;
; sOSage is distributed in the hope that it will be useful,             ;
; but WITHOUT ANY WARRANTY; without even the implied warranty of        ;
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ;
; GNU General Public License for more details.                          ;
;                                                                       ;
; You should have received a copy of the GNU General Public licenses    ;
; along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      ;
;-----------------------------------------------------------------------;

;-----------------------------------------------------------------------;
; Shutdown code for sOSage                                              ;
;                                                                       ;
; See :                                                                 ;
; http://www.delorie.com/djgpp/doc/rbinter/id/02/14.html                ;
; http://www.delorie.com/djgpp/doc/rbinter/id/18/14.html                ;
; http://www.delorie.com/djgpp/doc/rbinter/id/11/14.html                ;
;-----------------------------------------------------------------------;

halt: 

	mov	ax, 0x5301		; Connect real mode interface
	mov	bx, 0
	int	0x15

	mov	ax, 0x530e		; Set driver version to 1.2
	mov	bx, 0
	mov	cx, 0x0102
	int	0x15

	mov	ax, 0x5307		; Turn off system
	mov	bx, 0x0001
	mov	cx, 0x0003
	int	0x15

	hlt						; Halt
