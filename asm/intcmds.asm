;-----------------------------------------------------------------------;
; sOSage - an example OS for educational/hobbyist purposes              ;
; Copyright (C) 2020 Mark Williams                                      ;
;                                                                       ;
; This file is part of sOSage.                                          ;
;                                                                       ;
; sOSage is free software: you can redistribute it and/or modify        ;
; it under the terms of the GNU General Public License as published by  ;
; the Free Software Foundation, either version 3 of the License, or     ;
; (at your option) any later version.                                   ;
;                                                                       ;
; sOSage is distributed in the hope that it will be useful,             ;
; but WITHOUT ANY WARRANTY; without even the implied warranty of        ;
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ;
; GNU General Public License for more details.                          ;
;                                                                       ;
; You should have received a copy of the GNU General Public licenses    ;
; along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      ;
;-----------------------------------------------------------------------;

;-----------------------------------------------------------------------;
; sOSage internal commands                                              ;
;-----------------------------------------------------------------------;

lscmd:
	call	crlf
	mov 	si, cmdList
	call 	printMsg
	call 	crlf
	jmp	shell.nextCmd

poke:
	call	readAddress 		; read 4 char hex address into dx
	mov	si, dx 				; si needs to be used for indexing memory
	call	readDecimal			; read decimal value into bx
	mov	[si], bx 			; just copy one byte
	jmp	shell.nextCmd

dump:

	call	crlf

	call	readAddress

	push	dx					; will be popped into di later for output
	mov	dx, 0				; zero dx ready for division

	call	readDecimal		; read decimal value into bx

	pop	di

	mov	ax, bx
	mov	bx, 2				; divide by 2 as we have 2-byte words
	div	bx

	mov	bx, ax 				; bx = number of bytes to print out
	mov	cx, 0				; cx will count up to bx

	call	crlf				; start of dump's output routine
	.dump_loop:
	mov	ax, [di]
	ror	ax, 8
	mov	dl, 0x20
	push	bx					; because print2Hex uses bx...
	call	print2Hex
	pop	bx
	add	di, 2
	inc	cx
	cmp	cx, bx
	je		.dump_end

	push	cx					; see if we need a newline
	and	cx, 0x0007
	cmp	cx, 0
	jne	.dump_next

	call	crlf				; yup, newline needed

	.dump_next:
	pop	cx
	jmp	.dump_loop
	.dump_end:
	jmp	shell.nextCmd

save:

	mov	bx, [cmdArgs]
	dec	bx
	call	readDecimal		; read decimal value into bx
	mov	cl, bl			; Sector (counting from 1)

	mov	al, 1				; Sectors to write count

	call	calcSectorAddr
	call	writeSectors
	jmp	shell.nextCmd

load:
	call	readAddress		; read address into dx
	push	dx

	call	readDecimal		; read decimal value into bx
	mov	cl, bl			; Sector (counting from 1)

	;mov	bx, 0x8200			; into bx
	;mov	bx, dx			; copy address from dx into dx
	pop	bx

	mov	al, 1				; Sectors to read count
	call	readSectors
	jmp	shell.nextCmd

run:
	call	crlf
	call	readAddress
	call	dx
	jmp	shell.nextCmd
