;-----------------------------------------------------------------------;
; sOSage - an example OS for educational/hobbyist purposes              ;
; Copyright (C) 2020 Mark Williams                                      ;
;                                                                       ;
; This file is part of sOSage.                                          ;
;                                                                       ;
; sOSage is free software: you can redistribute it and/or modify        ;
; it under the terms of the GNU General Public License as published by  ;
; the Free Software Foundation, either version 3 of the License, or     ;
; (at your option) any later version.                                   ;
;                                                                       ;
; sOSage is distributed in the hope that it will be useful,             ;
; but WITHOUT ANY WARRANTY; without even the implied warranty of        ;
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ;
; GNU General Public License for more details.                          ;
;                                                                       ;
; You should have received a copy of the GNU General Public licenses    ;
; along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      ;
;-----------------------------------------------------------------------;

;-----------------------------------------------------------------------;
; Basic input and output routines for sOSage                            ;
;-----------------------------------------------------------------------;

printMsg:

	mov	ah, 0x0e		; 0x0e = BIOS function code for 'write character'

	.loop:
	lodsb					; Copies char into AL and increments SI 

	cmp	al, 0			; Our string is zero terminated...
	jz		.end			; so jump if zero

	int	0x10			; Otherwise, call BIOS (to print single char)
	jmp	.loop			; and then loop

	.end:
   ret

crlf:
	mov	ah, 0x0e
	mov	al, 0x0d		; CR
	int	0x10
	mov	al, 0x0a		; LF
	int	0x10
	ret

print2Hex:	
	push 	ax
	ror 	ax, 8			; print high byte first
	call 	printHex
	cmp 	dl, 0
	je .char2
	call 	printDelimiter
	.char2:
	pop 	ax 				; now print low byte
	call 	printHex
	cmp 	dl, 0
	je .print2Hex_end
	call 	printDelimiter
	.print2Hex_end:
	ret

printDelimiter:
	mov al, dl
	mov ah, 0x0e
	int 0x10
	ret

printHex:				; routine to output byte in al as 2 hex chars
	mov 	bl, al 		; copy al into bl so we don't lose data when we mask left nibble
	mov 	si, 2			; count down from 2

	.printHex_loop:
	rol 	bl, 4			; rotate bl left, so left-most nibble moves to right
	mov 	al, bl 		; copy to al
	and 	al, 0x0f		; mask out left nibble to get right nibble only

	cmp 	al, 0xa		; is the nibble >= 10?
	jge 	.printHex_alpha	; if so, output char will be alpha

	add 	al, 48		; add 48 to get ascii number char
	jmp 	.printHex_out

	.printHex_alpha:
	add 	al, 55		; add 55 to get ascii alpha char

	.printHex_out:
	mov 	ah, 0x0e
	int 	0x10
	dec 	si
	cmp 	si,0
	je 	.printHex_end
	jmp 	.printHex_loop

	.printHex_end:
	ret
