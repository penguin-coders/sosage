;-----------------------------------------------------------------------;
; sOSage - an example OS for educational/hobbyist purposes              ;
; Copyright (C) 2020 Mark Williams                                      ;
;                                                                       ;
; This file is part of sOSage.                                          ;
;                                                                       ;
; sOSage is free software: you can redistribute it and/or modify        ;
; it under the terms of the GNU General Public License as published by  ;
; the Free Software Foundation, either version 3 of the License, or     ;
; (at your option) any later version.                                   ;
;                                                                       ;
; sOSage is distributed in the hope that it will be useful,             ;
; but WITHOUT ANY WARRANTY; without even the implied warranty of        ;
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ;
; GNU General Public License for more details.                          ;
;                                                                       ;
; You should have received a copy of the GNU General Public licenses    ;
; along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      ;
;-----------------------------------------------------------------------;

;-----------------------------------------------------------------------;
; A shell for sOSage                                                    ;
;-----------------------------------------------------------------------;

shell:
	.nextCmd:
	; Clear command buffer
	mov	di, cmdBuff					; set up ready for stosb
	mov	ax, 0
	mov	cx, 32
	rep	stosb
	mov	di, cmdBuff					; set up ready for stosb
	mov	bx, cmdArgs

	call	crlf

	; prompt
	mov	si, prompt            
	call	printMsg

	.loop:
	;get the command
	
	mov	ah, 0                   ; Read char into al
	int	0x16 

	;cmp	al, 0x1b		; Escape pressed?
	;je	halt 

	cmp	al, 0x08		; Backspace (and delete)?
	jne	.store
	cmp	di, cmdBuff	; Are we at the beginning of the line?
	je		.loop
	call echo
	dec	di				; Overwrite deleted char in memory
	mov	al, 0
	call echo
	stosb
	dec di
	mov 	al, 0x08
	jmp	.ech

	; Copy into cmd buffer
	.store:
	stosb                  

	.ech:
	call echo

	cmp	al, 0x20 					; Did the user type a space?   
	jne	.checkReturn
	mov	[bx], di 	
	add	bx, 2              

	.checkReturn:					; Did the user hit return?
	cmp	al, 0x0d                    
	jne	.loop  

;lookup command number

	mov	ax, cmdList
	push	ax
	mov	ax, cmdBuff
	push	ax
	call	_getCmdNum

	; now process cmd based on command number
	
	cmp	ax, 0
	je		halt

	cmp 	ax, 1						; 1 = lscmd command
	je 	lscmd

	cmp	ax, 2						; 2 = poke command
	je		poke

	cmp	ax, 3						; 3 = dump command
	je		dump

	cmp	ax, 4						; 4 = save command
	je		save

	cmp	ax, 5						; 5 = load command
	je		load

	cmp	ax, 6						; 6 = run command
	je		run

	; try running as an external command
	mov	ax, cmdBuff
	push	ax
	call	_runExternalCmd

	cmp	ax, 0
	je		.prompt
	
	; if no commands found, output error msg
	call	crlf
	mov 	si, unrecognisedCmd            
	call	printMsg

	; prompt
	.prompt:
;	call	crlf
;	mov	si, prompt            
;	call	printMsg

	jmp	shell  

echo:
	push	bx						; Echo char
	mov	bx, 0                   
	mov	ah, 0x0e
	int	0x10
	pop	bx      
	ret
