;-----------------------------------------------------------------------;
; sOSage - an example OS for educational/hobbyist purposes              ;
; Copyright (C) 2020 Mark Williams                                      ;
;                                                                       ;
; This file is part of sOSage.                                          ;
;                                                                       ;
; sOSage is free software: you can redistribute it and/or modify        ;
; it under the terms of the GNU General Public License as published by  ;
; the Free Software Foundation, either version 3 of the License, or     ;
; (at your option) any later version.                                   ;
;                                                                       ;
; sOSage is distributed in the hope that it will be useful,             ;
; but WITHOUT ANY WARRANTY; without even the implied warranty of        ;
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ;
; GNU General Public License for more details.                          ;
;                                                                       ;
; You should have received a copy of the GNU General Public licenses    ;
; along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      ;
;-----------------------------------------------------------------------;

;-----------------------------------------------------------------------;
; Additional IO routines used to read in shell params.                  ;
;-----------------------------------------------------------------------;

readAddress:
	mov	dx, 0
	mov	cx, 4						; The 16-bit address will be 4 chars long (in hex)
	mov	bx, [cmdArgs]
	.readAddressLoop:

	shl	dx, 4
	mov	al, [bx]
	mov	ah, 0

	cmp	al, 97
	jge	.LCalpha

	cmp	al, 65
	jge	.UCalpha
	
	sub	al, 48
	jmp	.subdone

	.UCalpha:
	sub	al, 55
	jmp	.subdone

	.LCalpha:
	sub	al, 87

	.subdone:
	add	dx, ax
	inc	bx
	dec	cx
	cmp	cx, 0
	jne	.readAddressLoop
	ret

readDecimal:
	mov	cx, 0
	.nbytes:
	inc	bx							; increment pointer to point to next param
	cmp	byte [bx], 0x0d		; Jump out of loop if next char is CR
	je		.unstack

	mov	al, [bx]					; get the char and convert
	mov	ah, 0
	sub	al, 48
	push	ax
	inc	cx							; use cx to count the digits
	jmp	.nbytes

	.unstack:
	mov	bx, 0						; bx will hold the value
	mov	di, 1						; multiplier for each digit

	.unstack_loop:

	cmp	cx, 0						; jump out of loop if we've reached zero
	je		.end_nbytes

	pop	ax							; get digit from stack
	mul	di							; do place value multiplication
	add	bx, ax					; add to total

	mov	ax, 10
	mul	di
	mov	di, ax

	dec	cx							; decrement counter
	jmp	.unstack_loop

	.end_nbytes:
	ret
