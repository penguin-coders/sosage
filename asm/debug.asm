;-----------------------------------------------------------------------;
; sOSage - an example OS for educational/hobbyist purposes              ;
; Copyright (C) 2020 Mark Williams                                      ;
;                                                                       ;
; This file is part of sOSage.                                          ;
;                                                                       ;
; sOSage is free software: you can redistribute it and/or modify        ;
; it under the terms of the GNU General Public License as published by  ;
; the Free Software Foundation, either version 3 of the License, or     ;
; (at your option) any later version.                                   ;
;                                                                       ;
; sOSage is distributed in the hope that it will be useful,             ;
; but WITHOUT ANY WARRANTY; without even the implied warranty of        ;
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ;
; GNU General Public License for more details.                          ;
;                                                                       ;
; You should have received a copy of the GNU General Public licenses    ;
; along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      ;
;-----------------------------------------------------------------------;

debug:
;	ret	 							; Return straight away to disable debug

	push 	ax 						; Backup registers
	push 	bx
	push 	cx
	push 	dx
	push 	si
	push 	di

	push 	dx 						; Push registers we want to output
	push	cx
	push	bx
	push	ax
	push	di

	mov	si, regDI     			; Dump registers         
	call	printMsg
	pop 	ax
	call 	print2Hex

	mov	si, regAX
	call  printMsg
	pop 	ax
	call 	print2Hex

	mov   si, regBX
	call  printMsg
	pop 	ax
	call 	print2Hex

	mov   si, regCX
	call  printMsg
	pop 	ax
	call 	print2Hex

	mov   si, regDX
	call  printMsg
	pop 	ax
	call 	print2Hex

;	call	crlf

	pop 	di 						; Restore original register values
	pop 	si
	pop 	dx
	pop 	cx
	pop 	bx		
	pop 	ax
	ret
