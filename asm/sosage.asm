;-----------------------------------------------------------------------;
; sOSage - an example OS for educational/hobbyist purposes              ;
; Copyright (C) 2020 Mark Williams                                      ;
;                                                                       ;
; This file is part of sOSage.                                          ;
;                                                                       ;
; sOSage is free software: you can redistribute it and/or modify        ;
; it under the terms of the GNU General Public License as published by  ;
; the Free Software Foundation, either version 3 of the License, or     ;
; (at your option) any later version.                                   ;
;                                                                       ;
; sOSage is distributed in the hope that it will be useful,             ;
; but WITHOUT ANY WARRANTY; without even the implied warranty of        ;
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ;
; GNU General Public License for more details.                          ;
;                                                                       ;
; You should have received a copy of the GNU General Public licenses    ;
; along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      ;
;-----------------------------------------------------------------------;

%include 'asm/sectors.asm'

	org	0x7c00		; Address code will be loaded by the BIOS
	bits	16          ; Tell nasm we want to code for 16 bit real mode

start:
	cli					; Switch off interrupts

	; set segment registers 
	mov  bp, 0x7C00
	xor  ax, ax
	mov  ds, ax
	mov  es, ax
	mov  ss, ax
	mov  sp, bp  

	mov	si, bootMsg1 	; Pointer to msg, for use with lodsb
	call	printMsg 		; Call sub-rountine in basicIO.asm	

	mov	si, bootMsg2  	; Pointer to msg, for use with lodsb
	call	printMsg 		; Call sub-rountine in basicIO.asm		

	call	readOSsectors	; Call sub-rountine in diskIO.asm
	jmp 	sector1			; Jump to code in sector just loaded
	hlt

;-----------------------------------------------------------------------;
; Include sub-routines                                                  ;
;-----------------------------------------------------------------------;

%include 'asm/basicIO.asm'
%include 'asm/diskIO.asm'
%include 'asm/shutdown.asm'

;-----------------------------------------------------------------------;
; Messages                                                              ;
;-----------------------------------------------------------------------;

bootMsg1: db "Welcome to sOSage 0.3.1",13,10,10,0
bootMsg2: db "Booting sector 1...",13,10,0	
bootMsg3: db "Sector 1 loaded",13,10,10,0
bootMsg4: db "Command buffer address: 0x",0
bootMsg5: db "Command list address: 0x",0

regAX: db " | ax : ",0
regBX: db " | bx : ",0
regCX: db " | cx : ",0
regDX: db " | dx : ",0
regDI: db 13,10,"di : ",0

diskErrorMsg: db "I'm afraid there's been a bit of a disk-based cock-up.",13,10,0
unrecognisedCmd: db "Unrecognised command",13,10,0



;-----------------------------------------------------------------------;
; The last 2 bytes (i.e. 511 and 512) of the boot sector have to be set ; 
; to 0xaa55, because the BIOS will expect this 'magic number' to be     ; 
; there.                                                                ;
;                                                                       ;
; We'll fill the space up to byte 510 with zeros to ensure that 0xaa55  ;
; is in the right place.                                                ;
; '510 - ($-$$)' calculates the number of bytes to zero-fill,           ; 
; where $ is the current address                                        ;
; and $$ is the address of the start of the code.                       ;
;-----------------------------------------------------------------------;

	times 510 - ($-$$) db 0
	dw		0xaa55

;-----------------------------------------------------------------------;
; Sector 1                                                              ;
; N.B. cwrappers is included here to align with the start of the        ;
; sector. The sector1 label on the other hand is where the boot code    ;
; will jmp to.                                                          ;
;-----------------------------------------------------------------------;

%include 'asm/cwrappers.asm'

sector1:

	mov	si, bootMsg3                 
	call	printMsg

	mov	si, bootMsg4                
	call	printMsg
	mov	ax, cmdBuff
	call	print2Hex
	call	crlf

	mov	si, bootMsg5               
	call	printMsg
	mov 	ax, cmdList
	call 	print2Hex
	call 	crlf

;-----------------------------------------------------------------------;
; Include the shell, and internal commands                              ;
;-----------------------------------------------------------------------;

%include 'asm/shell.asm'
%include 'C/getCmdNum.asm'
%include 'C/runExternalCmd.asm'
%include 'asm/shellIO.asm'
%include 'asm/intcmds.asm'
%include 'asm/debug.asm'

;-----------------------------------------------------------------------;
; Shell related stuff:                                                  ;
; prompt, internal command list, bufferage                              ;
;-----------------------------------------------------------------------;

cmdList: 
	db "quit",32,32,32,32,"lscmd",32,32,32,"poke",32,32,32,32
	db "dump",32,32,32,32,"save",32,32,32,32,"load",32,32,32,32
	db "run",32,32,32,32,0
lenCmdList: db ($-cmdList)

cmdArgs: times 8 db 0
cmdBuff: times 32 db 0

prompt: db "$ ",0

;-----------------------------------------------------------------------;
; Include C functions                                                   ;
;-----------------------------------------------------------------------;

%include 'asm/div.asm' ; bcc generated asm, with manual tweaks
%include 'C/itoa10.asm'
%include 'C/strings.asm'
%include 'C/files.asm'
%include 'sFS/OScode/sFS.asm'

;-----------------------------------------------------------------------;
; As for the boot sector,                                               ;
; fill the rest of the last sector with zeros                           ;
;                                                                       ;
; The magic number at the end is 0x505A9E = sOSage                      ;
;-----------------------------------------------------------------------;

	times	sectors*512 - 3 - ($-$$) db 0
	
	db		0x50
	dw		0x9E5A
