;-----------------------------------------------------------------------;
; sOSage - an example OS for educational/hobbyist purposes              ;
; Copyright (C) 2020 Mark Williams                                      ;
;                                                                       ;
; This file is part of sOSage.                                          ;
;                                                                       ;
; sOSage is free software: you can redistribute it and/or modify        ;
; it under the terms of the GNU General Public License as published by  ;
; the Free Software Foundation, either version 3 of the License, or     ;
; (at your option) any later version.                                   ;
;                                                                       ;
; sOSage is distributed in the hope that it will be useful,             ;
; but WITHOUT ANY WARRANTY; without even the implied warranty of        ;
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ;
; GNU General Public License for more details.                          ;
;                                                                       ;
; You should have received a copy of the GNU General Public licenses    ;
; along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      ;
;-----------------------------------------------------------------------;

;-----------------------------------------------------------------------;
; (Floppy) Disk IO routines for sOSage                                  ;
;-----------------------------------------------------------------------;

readOSsectors:
	mov	al, sectors-1		; Sectors to read count
	mov	cl, 2					; Sector (counting from 1)
	call	calcSectorAddr
	; fall through into readSectors

readSectors:
	mov	ah, 0x02	; Function = read sectors
	mov	ch, 0		; Cylinder
	mov	dx, 0		; Drive (dl = 0 = floppy)
	mov	es, dx
	
	int	0x13

	jc diskError
	ret

writeSectors:
	mov	ah, 0x03	; Function = write sectors
	mov 	ch, 0		; Cylinder
	mov 	dl, 0		; Drive (0 = floppy)

	mov	dx, 0		; Drive (dl = 0 = floppy)
	mov	es, dx

	int	0x13

	jc diskError
	ret

diskError:
	mov	si, diskErrorMsg
	call	printMsg
	hlt

calcSectorAddr:		; Calculate the address of the sector from the sector number
	push	ax
	mov	ax, 0
	mov	al, cl
	mov	bx, 512
	mul	bx
	add	ax, 0x7a00
	mov	bx, ax
	pop	ax
	ret
