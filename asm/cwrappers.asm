;-----------------------------------------------------------------------;
; sOSage - an example OS for educational/hobbyist purposes              ;
; Copyright (C) 2020 Mark Williams                                      ;
;                                                                       ;
; This file is part of sOSage.                                          ;
;                                                                       ;
; sOSage is free software: you can redistribute it and/or modify        ;
; it under the terms of the GNU General Public License as published by  ;
; the Free Software Foundation, either version 3 of the License, or     ;
; (at your option) any later version.                                   ;
;                                                                       ;
; sOSage is distributed in the hope that it will be useful,             ;
; but WITHOUT ANY WARRANTY; without even the implied warranty of        ;
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ;
; GNU General Public License for more details.                          ;
;                                                                       ;
; You should have received a copy of the GNU General Public licenses    ;
; along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      ;
;-----------------------------------------------------------------------;

;-----------------------------------------------------------------------;
; The following jmp statements simply jmp to the wrapper functions      ;
; defined in this file. As the code in this file will be aligned with   ;
; the start of sector 1, the addresses will be fixed even if the        ;
; addresses being jumped to may change. This facilitates linking in     ;
; code assembled separately (without the need for an actual linker).    ;
;-----------------------------------------------------------------------;

; short jumps
	jmp _printMsg
	nop
	;jmp _readWriteSectors
	;nop
	;jmp _writeSectors
	;nop

; near jumps
_crlf: ; label here is for the benefit of internal C functions
	jmp crlf
	jmp _itoa10
	jmp _strcpy
	jmp _strrchr
	jmp _memcpy
	jmp _fopen
	jmp _fread
	jmp _fseek
	jmp _fwrite
	jmp _read_index_blk
	jmp _get_last_index_blk

_printMsg:
	push	bp
	mov	bp,sp
	push	di
	push	si
	mov	si, [bp+4]		; Copy string pointer from stack
	call printMsg
	pop	si
	pop	di
	pop	bp
	ret

_readWriteSectors:
	push	bp
	mov	bp,sp
	push	di
	push	si
	mov	bx, [bp+10]		; Copy sector from stack to...
	mov	cl, bl			; cl
	mov	bx, [bp+4]		; Copy address from stack to bx
	mov	al, [bp+12]		; Sectors to read count

	mov	ah, [bp+14] 	; 0x02 = read, 0x03 = write
	mov	ch, [bp+6]		; Cylinder
	mov	dx, 0				; Drive (dl = 0 = floppy)
	mov	es, dx
	mov	dh, [bp+8]		; Head
	
	int	0x13

	jc diskError

	pop	si
	pop	di
	pop	bp
	ret

_writeSectors:
	push	bp
	mov	bp,sp
	push	di
	push	si
	mov	bx, [bp+6]		; Copy sector from stack to...
	mov	cl, bl			; cl
	mov	bx, [bp+4]		; Copy address from stack to bx
	mov	al, [bp+8]		; Sectors to write count
	call	writeSectors
	pop	si
	pop	di
	pop	bp
	ret

_run_code_at_addr:
	push 	bp
	mov	bp,sp
	push	di
	push	si
	mov	dx, [bp+4]
	call	dx
	pop	si
	pop	di
	pop 	bp
	ret
