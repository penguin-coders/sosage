/************************************************************************
 * sOSage - an example OS for educational/hobbyist purposes              *
 * Copyright (C) 2022 Mark Williams                                      *
 *                                                                       *
 * This file is part of sOSage.                                          *
 *                                                                       *
 * sOSage is free software: you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * sOSage is distributed in the hope that it will be useful,             *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      *
 *************************************************************************/

#include <stdio.h>
#include <math.h>
#include "fs.h"

int main() {

	FILE *outfile;
	filesystem fs;

	fs.magic_num[0] = 's';
	fs.magic_num[1] = 'F';
	fs.magic_num[2] = 'S';
	fs.magic_num[3] = '>';
	fs.end[0] = '<';
	fs.end[1] = 's';
	fs.end[2] = 'F';
	fs.end[3] = 'S';

	fs.version = FSVERSION;
	fs.block_size = BLKSIZE;
	fs.num_blocks = NUMBLKS; 

	// Initialise fsmap and fsptrs
	for(int c=0; c<FSMAP; c++) {
		fs.fsmap[c] = 0;
	}
	for(int c=0; c<FSPTRS; c++) {
		fs.fsptrs[c] = 0;
	}

	// Mark blocks used by fs struct and OS sectors
	unsigned short used_blocks = (unsigned short) ceil((double)sizeof(fs) / (double)BLKSIZE) + OSSECTORS;
	for(int blk=0; blk<used_blocks; blk++) { 
		mark_block_used(&fs,blk);	
		set_block_ptr(&fs, blk, blk+1);
	}
	
	// Setup the index block
	fs.index_block = used_blocks;
	mark_block_used(&fs, used_blocks);	

	// Write struct out to filesystem file
	outfile = fopen(FSFILE, "r+b");
	fseek(outfile, BLKSIZE*OSSECTORS, SEEK_SET); // skip over OS sectors
	fwrite(&fs, sizeof(fs), 1, outfile);

	// Zero the index block
	int zero_count = BLKSIZE*(used_blocks+1)-sizeof(fs);
	for(int i=0; i<zero_count; i++)
		fputc(0, outfile);

	fclose(outfile);
	return 0;

}
