/************************************************************************
 * sOSage - an example OS for educational/hobbyist purposes              *
 * Copyright (C) 2022 Mark Williams                                      *
 *                                                                       *
 * This file is part of sOSage.                                          *
 *                                                                       *
 * sOSage is free software: you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * sOSage is distributed in the hope that it will be useful,             *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      *
 *************************************************************************/

#define FNAMELEN 10 
#define RECSPERBLK 31

#include "fs.h"
#include <stdio.h>
#include <string.h>

typedef struct {
	char filename[FNAMELEN];
	unsigned short int start_blk;
#ifdef __BCC__
	unsigned long length;
#else
	unsigned int length;	
#endif
} dirrec;

typedef struct {
	dirrec recs[RECSPERBLK];
	unsigned short int next_rec;
	struct index_blk *next_blk;
} index_blk;

void read_index_blk(); //FILE *fsfile, unsigned int blk, index_blk *indx);
void write_index_blk(); //FILE *fsfile, unsigned int blk, index_blk *indx); 
int get_last_index_blk(); //filesystem *fs);
void add_to_index(); //jFILE *fsfile, filesystem *fs, char *fname, unsigned int blk, unsigned int length);
