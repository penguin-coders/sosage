/************************************************************************
 * sOSage - an example OS for educational/hobbyist purposes              *
 * Copyright (C) 2022 Mark Williams                                      *
 *                                                                       *
 * This file is part of sOSage.                                          *
 *                                                                       *
 * sOSage is free software: you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * sOSage is distributed in the hope that it will be useful,             *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      *
 *************************************************************************/

#include "fs.h"
#include <stdio.h>

unsigned int pow2(n) 
unsigned int n;
{
	int i, r=1;
	for(i=0; i<n; i++) {
		r *= 2;
	}
	return r;
}

void set_bit(pchar, bitnum)
unsigned char *pchar; 
unsigned short int bitnum; 
{
	*pchar |= (unsigned char) pow2(bitnum);
}

void clear_bit(pchar, bitnum) 
unsigned char *pchar;
unsigned short int bitnum; 
{
	*pchar &= ~(unsigned char) pow2(bitnum);
}

void mark_block_used(fs, blk) 
filesystem *fs; 
int blk; 
{
#ifdef DEBUG 
	printf("Marking block %u : ", blk);
#endif
	set_bit(&(fs->fsmap[blk/8]),blk%8);
#ifdef DEBUG 
	printf("%x\n",fs->fsmap[blk/8]);
#endif
}

void mark_block_free (fs, blk) 
filesystem *fs; 
int blk; 
{
	clear_bit(&(fs->fsmap[blk/8]),blk%8);
}

void set_block_ptr(fs, blk, val) 
filesystem *fs; 
unsigned int blk; 
unsigned int val; 
{
		
	int i;
	numchar new_bytes, old_bytes, bmask;
	unsigned int start_bit = blk*BITSPERBLK;
	unsigned int start_byte = start_bit / 8;
	unsigned int offset_bits = start_bit % 8;
  
	new_bytes.n = val;
	new_bytes.n <<= offset_bits;

	old_bytes.ch[0] = fs->fsptrs[start_byte];
	old_bytes.ch[1] = fs->fsptrs[start_byte + 1];

	bmask.n = 0xFF00;
	bmask.n <<= offset_bits-8;

	for(i=0; i<offset_bits; i++) {
		bmask.n <<= 1;
		bmask.n++;
	}

	new_bytes.n += (old_bytes.n & bmask.n);
#if DEBUG 
	printf("set_block_ptr %u %u : new_bytes %02x %02x\n", blk, val, new_bytes.ch[0], new_bytes.ch[1]);
#endif

	fs->fsptrs[start_byte] = new_bytes.ch[0];
	fs->fsptrs[start_byte + 1] = new_bytes.ch[1];
}

int next_free_block(fs) 
filesystem *fs; 
{
	int ch, bit;
	for(ch=0; ch<FSMAP; ch++) {
#if DEBUG 
		printf("byte %u : %x\n",ch,fs->fsmap[ch]);
#endif
		if(fs->fsmap[ch]!=0xFF) {
			unsigned char byte = fs->fsmap[ch];
			for(bit=0; bit<8; bit++) {
				if((byte & 1) == 0) {
#if DEBUG 
					printf("Returning free block %u\n", ch*8+bit);
#endif
					return ch*8+bit;
				}
				byte >>= 1;
			}
		}
	}
	return -1; // no free space!
}

int next_file_block(fs, curr_blk) 
filesystem *fs; 
unsigned int curr_blk; 
{
	
	numchar blk_ptr;
	unsigned int start_bit = curr_blk*BITSPERBLK;
	unsigned int start_byte = start_bit / 8;
	unsigned int offset_bits = start_bit % 8;
  
	blk_ptr.ch[0] = fs->fsptrs[start_byte];
	blk_ptr.ch[1] = fs->fsptrs[start_byte + 1];

#if DEBUG 
	printf("initial blk_ptr: %02x %02x\n",blk_ptr.ch[0],blk_ptr.ch[1]);
#endif
	
	blk_ptr.n >>= offset_bits;
#if DEBUG 
	printf("shifted blk_ptr: %02x %02x\n",blk_ptr.ch[0],blk_ptr.ch[1]);
#endif
	blk_ptr.n &= 0x0FFF; // mask out bits relating to next blk
#if DEBUG 
	printf("masked  blk_ptr: %02x %02x\n",blk_ptr.ch[0],blk_ptr.ch[1]);
#endif

	return blk_ptr.n;
}
