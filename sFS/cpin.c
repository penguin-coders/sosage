/************************************************************************
 * sOSage - an example OS for educational/hobbyist purposes              *
 * Copyright (C) 2022 Mark Williams                                      *
 *                                                                       *
 * This file is part of sOSage.                                          *
 *                                                                       *
 * sOSage is free software: you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * sOSage is distributed in the hope that it will be useful,             *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      *
 *************************************************************************/

#include <stdio.h>
#include <math.h>
#include <sys/stat.h>
#include <string.h>
#include "fs.h"
#include "index.h"

int main(int argc, char *argv[]) {

	if(argc!=2) {
		fprintf(stderr, "Incorrect number of arguments.\n");
		return 1;
	}

	FILE *fsfile;
	FILE *infile;
	int num_blocks, num_pad_bytes;
  	int curr_block, prev_block, save_block;
	struct stat fstats;
	filesystem fs;
	unsigned char block[BLKSIZE];
	char *infname = argv[1];

	// Get input file stats and open
	if (stat(infname, &fstats) == 0) {
		num_blocks = (int) ceil((double)fstats.st_size / (double)BLKSIZE);
		num_pad_bytes = num_blocks*BLKSIZE - fstats.st_size;
		infile = fopen(infname, "rb");
	} else {
		printf("Unable to get file size.\n");
		return 1;
	}

	// Read in filesystem struct
	fsfile = fopen(FSFILE, "r+b");
	fseek(fsfile, BLKSIZE*OSSECTORS, SEEK_SET); // skip over OS sectors
	fread(&fs, sizeof(fs), 1, fsfile);

	// Copy blocks into filesystem
	for(int c=0; c<num_blocks; c++) {
		// Set up next block
		prev_block = curr_block;	
		curr_block = next_free_block(&fs);
		if(c==0)
			save_block = curr_block;
		mark_block_used(&fs, curr_block);	
		if(c>0) 
			set_block_ptr(&fs, prev_block, curr_block);
		// Read in block & zero pad if necessary
		if(fread(block, sizeof(block), 1, infile)==0) 
			for(int i=BLKSIZE-num_pad_bytes; i<BLKSIZE; block[i++]=0);
		// Write out block
		fseek(fsfile, BLKSIZE*curr_block, SEEK_SET);
		fwrite(block, sizeof(block), 1, fsfile);
	}

	// Add entry to directory index
#if DEBUG
		curr_block = save_block;
		while(curr_block!=0) {
			printf("curr_block: %u\n",curr_block);
			curr_block = next_file_block(&fs, curr_block);
		}
#endif	
	add_to_index(fsfile, &fs, infname, save_block, fstats.st_size);

	// Save filesystem struct
	fseek(fsfile, BLKSIZE*OSSECTORS, SEEK_SET);
	fwrite(&fs, sizeof(fs), 1, fsfile);

	fclose(fsfile);
	fclose(infile);

	return 0;
}

