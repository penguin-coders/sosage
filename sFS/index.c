/************************************************************************
 * sOSage - an example OS for educational/hobbyist purposes              *
 * Copyright (C) 2022 Mark Williams                                      *
 *                                                                       *
 * This file is part of sOSage.                                          *
 *                                                                       *
 * sOSage is free software: you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * sOSage is distributed in the hope that it will be useful,             *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with sOSage.  If not, see <https://www.gnu.org/licenses/>.      *
 *************************************************************************/

#include "index.h"

void read_index_blk(fsfile, blk, indx) 
FILE *fsfile; 
unsigned int blk; 
index_blk *indx; 
{
	fseek(fsfile, BLKSIZE*blk, SEEK_SET);
	fread(indx, sizeof(index_blk), 1, fsfile);
}

void write_index_blk(fsfile, blk, indx) 
FILE *fsfile; 
unsigned int blk;
index_blk *indx;
{
	fseek(fsfile, BLKSIZE*blk, SEEK_SET);
	fwrite(indx, sizeof(index_blk), 1, fsfile);
}

int get_last_index_blk(fs) 
filesystem *fs; 
{
	int blk = fs->index_block;
	return blk;	
}

void add_to_index(fsfile, fs, fname, blk, length) 
FILE *fsfile; 
filesystem *fs; 
char *fname; 
unsigned int blk; 
unsigned int length; 
{
	int last_index_blk;
	index_blk indx;
	fname = strrchr(fname,'/')+1;

	// read in relevant index block
	last_index_blk = get_last_index_blk(fs);
   read_index_blk(fsfile, last_index_blk, &indx);	
		
	// update index
	strcpy(indx.recs[indx.next_rec].filename, fname);
	indx.recs[indx.next_rec].start_blk = blk;
	indx.recs[indx.next_rec].length = length;
	indx.next_rec++;

	// save back to disk
	write_index_blk(fsfile, last_index_blk, &indx);
#ifdef DEBUG 
	printf("\nFile written to filesystem.\nStart block: %u\nStart sector: %u\n\n", blk, blk+1);
#endif
}
