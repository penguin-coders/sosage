# Change log

## v0.3.1 - 2022-04-09

Re-engineered the shell to look for typed commands in the filesystem and, if found, load and run them. The user no longer needs to keep track of sectors and memory addresses.


**Added:**
- run_code_at assembler function in cwrappers, for running loaded code when calling from C

**Changed:**
- The shell

**Bug-fixes:**
- Fixed a bug in the shell where it sometimes displayed the prompt twice.

## v0.3.0 - 2022-04-07

Completed integration of sFS into sOSage. 

The sOSage image file, sosage.img, was already formatted using sFS, and files could be copied in using the cpin utility, but sOSage itself was not aware of it's filesystem. 

Currently, it's still necessary to keep track of sectors and memory addresses when loading and running programs, but the filesystem integration paves the way for some shell re-engineering which will eliminate the need for this.

Also, there is now a working ls command which lists files within the filesystem.

**Added:**
- ls command (in extC)
- sFS code compiled for sOSage

**Changed:**
- sFS source code (to be bcc compatible)

## v0.2.10 - 2022-04-04

Implemented fwrite

**Added:**
- fwrite

**Changed:**
- readSectors wrapper function changed to readWriteSectors

## v0.2.9 - 2022-04-03

Further work on fread and fseek

**Added:**
- Added test entry in Makefile
- Added code to handle disk geometry
- Initial implementation of fseek

**Changed:**
- Updates to testFileIO for testing fseek and multiple structs
- Amended fread to cope with offsets not aligned to sectors
- Updated fread to update offset bytes

**Bug-fixes:**
- Work-around for current lack of malloc function 
- Bug-fixed fread function.
- Additional rule for ble / jle in bcc2nasm.pl

## v0.2.8 - 2022-03-28

Various minor changes and refactorings made while debugging and working towards incorporating the filesystem into the OS.

**Added:**
- Initial versions of fopen, fread and the FILE struct
- New sizer.c test program to output sizes of data types in bytes
- Added monitor option for sOSboot.sh

**Changed:**
- Moved functions which can be called from external C programs into header files to form an initial standard library
- Made jmps to C functions in OS far jumps
- Added code to initialise segment registers
- readSector and writeSector routines changed to readSectors and writeSectors, with support to read/write multiple sectors
- Merged bcc2nasm.pl and symlinked from extC/ to C/
- Added pow2 function to avoid the need to use math.h in sFS

**Bug-fixes:**
- Refactored itoa10 into 2 functions to ensure division is unsigned
- Bug-fixed itoa10 for case when n=0

## v0.2.7 - 2022-03-24

Added some string functions which would normally be in the standard library in string.h

**Added:**
- Implementation of strcpy
- Implementation of strrchr

## v0.2.6 - 2022-03-24

New C functions and wrapper funtions as well as code improvements / refactoring.

**Changed:**
- Renamed cmsg to testEctC in extC folder
- Improved code for handling C wrappers
- Number of OS sectors now defined in one place

**Added:**
- Added C wrapper for writeSector
- Added itoa10 as new C function

## v0.2.5 - 2022-03-19

Added C wrappers for crlf and readSector functions. Also amended external C program to test these.
Added lsindex utility for listing the files with a sOSage image file.

**Changed:**
- Added code to cmsg.c to test new wrapper functions, as well as structs and pointers.

**Added:**
- C wrapper for crlf
- C wrapper for readSector
- lsindex utility

## v0.2.4 - 2022-03-13

Renamed ls to lscmd so that in future versions ls can be used to list files within the filesystem. lscmd will list internal commands (which are not stored as separate executables within the filesystem).

**Changed:**
- ls changed to lscmd

## v0.2.3 - 2022-03-13

Incorporated the sFS filesystem into sOSage.

**Added:**
- sFS filesystem
- sFS format program to write filesystem structures to image file
- sFS cpin program to copy files into the filesystem in the image file

**Changed:**
- load command now requires an address parameter

## v0.2.2 - 2022-02-14

Renamed the C folder to extC and added a new C folder for C code which is part of the OS. Bug fixed a few shell bugs using C.

**Added:**
- C folder of OS code

**Changed:**
- Existing C folder renamed

**Bug-fixes:**
- Shell: typos now correctable with delete key
- Shell: bug fixed code to determine command number
- Shell: can only delete back to prompt

## v0.2.1 - 2022-02-06

Added run command along with a cwrappers.asm file to facilitate running seperately compiled C programs. 

A C folder has also been added with an example C program, Makefile and a script to pre-process bcc output for input into nasm.

**Added:**
- run command
- cwrappers.asm
- C folder with example program

## v0.2.0 - 2020-12-12

Changed save command to handle a parameter, so that arbitrary sectors can be saved. 

With load, poke and save, the disk (image) containing sOSage can now be edited from within sOSage - bumping minor version.

**Changed:**
- save command - now saves the sector specified by a param

## v0.1.11 - 2020-12-12

Added load command to load a sector from disk to RAM

**Added:**
- load command

## v0.1.10 - 2020-12-10

Added save command. Currently, it is hard-wired to save the 3rd sector back to the disk image, but will be expanded so that any sector can be specified.

**Added:**
- save command

## v0.1.9 - 2020-12-10

Added poke command and refactored out some common code. Code now extends to 3 sectors, so refactored to allow for that as well.

**Added:**
- poke command

**Changed:**
- diskIO routines (to allow for reading in multiple OS sectors)
- refactored dump command so that some of the code could be used by poke

**Removed:**
- hex command

## v0.1.8 - 2020-12-08

Third iteration of dump command, which now takes a 2nd parameter specifying the number of bytes to dump.

**Changed:**
- dump command

## v0.1.7 - 2020-12-08

Second iteration of the dump command, which now takes as a parameter a 4-digit hex address from which to commence dumping. N.B. This is an address in memory, rather than on disk.

**Changed:**
- Updated shell so that commands can take parameters
- Updated dump to take a parameter

## v0.1.6 - 2020-12-08

First iteration of the dump command - currently hard-wired to print 16 bytes of the command buffer.

**Added:**
- dump command
- boot messages to show addresses

**Changed:**
- Some incorrect version numbers in the CHANGELOG. D'Oh!

## v0.1.5 - 2020-12-08

Added a debug routing which prints out register values. To use, insert `call debug` at any point in the code.

**Added:**
- print2Hex routine to print 16 bit value as 2 bytes in hex
- debug routine

## v0.1.4 - 2020-12-08

Added printHex routine, which will form the basis of a dump command to hexdump memory (not yet implemented).

**Added:**
- printHex sub-routine
- hex command to test printHex routine

## v0.1.3 - 2020-12-08

**Added:**
- Unrecognised command error message

**Bug fixes:**
- Stopped sOSage crashing when enter key pressed without typing command

## v0.1.2 - 2020-12-08

sOSage now has 2 commands - ls and quit.

**Added:**
- crlf sub-routine to print newlines
- intcmds.asm for handlind internal commands
- cmdList in data section, defining command

**Changed:**
- Amended shell loop to handle commands instead of just echoing.

**Removed:**
- Commented out code which halts the machine on pressing Escape (because we now have a quit command).

## v0.1.1 - 2020-12-08

**Added:**
- Error message for disk errors

## v0.1.0 - 2020-12-08

Added some disk IO code to load in the second sector (sector 1) - this will allow the code to be extended beyond the boot sector (sector 0).

**Added:**
- readSector sub-routine

**Changed:**
- Moved shell.asm code into sector 1

## v0.0.4 - 2020-12-08

Version to test the shutdown routine. Hitting the escape key will shutdown the machine.

**Added:**
- Shutdown routine

## v0.0.3 - 2020-12-08

Text can now be typed at the prompt and will be echoed back after pressing enter.

**Added:**
- Shell loop
- Code to read in and echo text

## v0.0.2 - 2020-12-07

sOSage now displays a prompt. (It's not yet possible to type anything, but it shows the prompt.)

**Added:**
- Code to display a prompt

**Changed:**
- Moved code to print messages into printMsg sub-routine.

## v0.0.1 - 2020-12-07

First bootable version of sOSage - simply boots to a "Welcome to sOSage" message.

**Added:**
- Initial Assembler source file
- A Makefile for assembling with `make`
- A shell script for booting with QEMU
- A .gitignore


## v0.0.0 - 2020-12-07

**Added:**
- README
- LICENCE
- CHANGELOG
