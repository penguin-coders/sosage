#!/bin/bash
if [ $# -eq 1 ] && [ $1 == "-m" ]
then
	qemu-system-x86_64 -monitor stdio -drive format=raw,file=sosage.img,index=0,if=floppy 
else
	qemu-system-x86_64 -drive format=raw,file=sosage.img,index=0,if=floppy & 
fi
